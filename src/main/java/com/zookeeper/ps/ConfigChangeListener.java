package com.zookeeper.ps;

/**
 * 配置事件监听
 */
public interface ConfigChangeListener {

	public abstract void change(String p1, Object value);
	
}
